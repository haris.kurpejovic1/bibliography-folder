import os
from Products.CMFCore.permissions import AddPortalContent
from example.bibliography_folder import _
from example.bibliography_folder.parsers.bibtex import BibTeXParser


ADD_CONTENT_PERMISSION = AddPortalContent
PROJECTNAME = "GEMIBibliographyDexterity"

USE_EXTERNAL_STORAGE = True

ENTRY_TYPES = 'bibtex_types'

FOLDER_TYPES = (
    'BibliographyFolder',
    'LargeBibliographyFolder',
)

PREPRINT_SERVERS = ('None:',
                    'arXiv:http://arxiv.org',
                    'CogPrints:http://cogprints.ecs.soton.ac.uk/',
                    )

REFERENCE_TYPES = (
    'ArticleReference',
    'BookletReference',
    'BookReference',
    'ConferenceReference',
    'InbookReference',
    'IncollectionReference',
    'InproceedingsReference',
    'ManualReference',
    'MastersthesisReference',
    'MiscReference',
    'PhdthesisReference',
    'PreprintReference',
    'ProceedingsReference',
    'TechreportReference',
    'UnpublishedReference',
    'WebpublishedReference',
    )

ZOPE_TEXTINDEXES = (
    'TextIndex',
    'ZCTextIndex',
    'TextIndexNG2',
    'TextIndexNG3',
)

IDENTIFIER_TYPES = (
    "DOI",
    "ISBN",
    "PMID",
    "ASIN",
    "PURL",
    "URN",
    "ISSN",
)

INPUT_ENCODINGS = (
    "UTF-8",
)

PARSER_FORMATS = {
    'bibtex': {
        'format': 'BibTeX',
        'description': _("A specific parser to process input in BibTeX-format."),
        'export_description': _("Export to native BibTeX format (with LaTeX escaping)"),
        'parser': BibTeXParser
    },
}



"""
if USE_EXTERNAL_STORAGE:
    try:
        import Products.ExternalStorage
    except ImportError:
        LOG(PROJECTNAME, PROBLEM, 'ExternalStorage N/A, falling back to AnnotationStorage')
        USE_EXTERNAL_STORAGE = False
"""

