from example.bibliography_folder import _



def format_author_name(author, format="{L} {f}"):

    firstname = author.get('firstname')
    lastname = author.get('lastname')
    middlename = author.get('middlename')

    str = format.format(
        L=(lastname if lastname is not None else ''),
        l=(lastname[0] if lastname is not None else ''),
        F=(firstname if firstname is not None else ''),
        f=(firstname[0] if firstname is not None else ''),
        M=(middlename if middlename is not None else ''),
        m=(middlename[0] if middlename is not None else ''),
    )

    return str.strip()