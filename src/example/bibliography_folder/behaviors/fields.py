from example.bibliography_folder.vocabularies.vocabulary import identifiertypes
from plone.dexterity.interfaces import IDexterityContent
from example.bibliography_folder import _
from plone.autoform.interfaces import IFormFieldProvider
from zope import schema
from plone.supermodel import model
from plone.autoform.interfaces import IFormFieldProvider
from z3c.form.browser.radio import RadioFieldWidget
from plone.autoform import directives
from collective.z3cform.datagridfield import DictRow
from collective.z3cform.datagridfield import DataGridFieldFactory
from zope.interface import provider
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary


@provider(IFormFieldProvider)
class IJournalField(model.Schema):

    journal = schema.TextLine(
        title=_(u'Journal'),
        description=_("A journal name (should be the full journal name)."),
        required=False,
    )

@provider(IFormFieldProvider)
class IVolumeField(model.Schema):
    
    volume = schema.TextLine(
        title=_(u'Volume'),
        description=_("The volume of a journal, multivolume book work etc. In most cases it should suffice to use a simple number as the volume identifier."),
        required=False,
    )

@provider(IFormFieldProvider)
class INumberField(model.Schema):

    number = schema.TextLine(
        title=_("Number"),
        description=_("The number of a journal, proceedings, technical report etc. Issues of journals, proceedings etc. are usually identified by volume and number; however, issues of the same volume will often be numbered preceedingly which often makes the specification of a number optional. With technical reports, the issuing organization usually also gives it a number."),
        required=False,
    )

@provider(IFormFieldProvider)
class IPagesField(model.Schema):

    pages = schema.TextLine(
        title=_("Pages"),
        description=_("A page number or range of numbers such as '42-111'; you may also have several of these, separating them with commas: '7,41,73-97'."),
        required=False,
    )

@provider(IFormFieldProvider)
class IIdentifier(model.Schema):
    """
    Interface class defining the properties of an identifier entry
    """
    identifier_type = schema.Choice(
        title=_("Identifier"),
        required=False,
        vocabulary=identifiertypes(),
    )

    identifier_value = schema.TextLine(
        title=_("Value"),
        required=False,
    )

@provider(IFormFieldProvider)
class IIdentifierSchema(model.Schema):
    identifiers= schema.List(
        title=_("Identifiers"),
        value_type=DictRow(title=_("Table"), schema=IIdentifier),
        default=[],
        required=False,
    )
    directives.widget("identifiers", DataGridFieldFactory)

@provider(IFormFieldProvider)
class IPublicationMonthField(model.Schema):
    publication_month = schema.TextLine(
        title=_("Publication Month"),
        description=_("Month of publication (or writing, if not published)."),
        required=False,
    )

@provider(IFormFieldProvider)
class INoteField(model.Schema):
    note = schema.Text(
        title=_("Note"),
        description=_("Any additional information that can help the reader. The first word should be capitalized."),
        required=False,
    )

@provider(IFormFieldProvider)
class IAnnotationField(model.Schema):
    annotation = schema.Text(
        title=_(" Personal annotation "),
        description=_("Any annotation that you do not wish to appear in rendered (BibTeX) bibliographies."),
        required=False,
    )
