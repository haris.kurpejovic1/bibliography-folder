from plone.dexterity.content import Item
from plone.supermodel import model
from example.bibliography_folder import _
from example.bibliography_folder.helpers import format_author_name
from zope import schema
from plone.autoform import directives
from plone.supermodel.directives import fieldset
from plone.autoform.interfaces import IFormFieldProvider
from z3c.form.browser.radio import RadioFieldWidget
from z3c.form import interfaces as z3cinterfaces
from plone.autoform.interfaces import IFormFieldProvider
from plone.namedfile.field import NamedFile
from zope.interface import provider
from plone.app.z3cform.wysiwyg import WysiwygFieldWidget
from zope.interface import Invalid
from plone.app.textfield import RichText

from example.bibliography_folder.content.pdf import IPdfFile
from plone.namedfile.field import NamedFile

from z3c.relationfield.schema import RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder

from plone.app import z3cform
from plone.app.z3cform.widget import AjaxSelectFieldWidget

explanation_html=_("""\
There are several ways to make reference to the original paper:
<ul>
<li>A link to an online version</li>
<li>A link to a printable (pdf) version</li>
<li>Upload a printable (pdf) file</li>
</ul>
""")

def pdf_filetype_constraint(value):
    """ Check if the supplied filetype is a PDf.
    """
    if not (value.contentType == 'application/pdf'):
        raise Invalid("Please only upload Pdfs.")
    return True
    
@provider(IFormFieldProvider)
class ICoreSchema(model.Schema):
    
    """
    Core schema file for bibliography item type
    @todo: documentation
    """

    publication_year = schema.TextLine(
        title=_(u'Publication Year'),
    )

    abstract = RichText(
        title=_("Abstract"),
        description=_("An abstract of the referenced publication. Please contact your portal's reviewers if unsure about the site's default language for abstracts in bibliographical references."),
        readonly=True
    )

    directives.mode(explain_links='display')
    explain_links = RichText(
        title="",
        default_mime_type='text/html',
        output_mime_type='text/x-html-safe',
        allowed_mime_types=('text/html',),
        default=explanation_html,
    )

    publication_url = schema.TextLine(
        title=_(u'Publication URL'),
        required=False,
    )

    pdf_url = schema.TextLine(
        title=_(u'PDF URL'),
        required=False,
    )

    pdf_reference = RelationChoice(
        title=_("Reference to PDF"),

        # source=ObjPathSourceBinder(),
        vocabulary='plone.app.vocabularies.Catalog',
        required=False,
    )

    pdf_file = NamedFile(
        title=_("Upload PDF File"),
        description=_("If not in conflict with any copyright issues, use this field to upload a printable version (PDF file) of the referenced resource."),
        required=False,
        constraint=pdf_filetype_constraint
    )

    fieldset(
        'full-text',
        label=_(u'Full Text'),
        fields=('explain_links', 'publication_url', 'pdf_url', 'pdf_file', 'pdf_reference'),
    )

    # uploaded_pdfFile
    # is_duplicate_of

    # ===== TrailingSchema
    # identifiers
    # publication_month
    # note
    # annote
    # additional
