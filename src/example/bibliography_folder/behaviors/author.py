from plone.dexterity.interfaces import IDexterityContent
from example.bibliography_folder import _
from plone.autoform.interfaces import IFormFieldProvider
from zope import schema
from plone.supermodel import model
from plone.autoform.interfaces import IFormFieldProvider
from z3c.form.browser.radio import RadioFieldWidget
from plone.autoform import directives
from collective.z3cform.datagridfield import DictRow
from collective.z3cform.datagridfield import DataGridFieldFactory
from zope.interface import provider


@provider(IFormFieldProvider)
class IAuthor(model.Schema):
    """
    Interface class defining the properties of an author
    """

    firstname = schema.TextLine(
        title='First Name',
        required=False,
        )

    lastname = schema.TextLine(
        title='Last Name',
        required=False,
    )

    middlename = schema.TextLine(
        title='Middle Name',
        required=False,
    )

    homepage = schema.TextLine(
        title='Home Page',
        required=False,
    )


@provider(IFormFieldProvider)
class IAuthorSchema(model.Schema):

    """
    Schema for defining the authors attribute for article types
    """

    authors = schema.List(
        title=_(u'Authors'),
        value_type=DictRow(title=_(u'Table'), schema=IAuthor),
        default=[],
        required=False,
    )
    directives.widget('authors', DataGridFieldFactory)

    

