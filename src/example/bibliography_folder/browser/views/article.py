from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from html.parser import HTMLParser
from example.bibliography_folder import _



class ArticleReference(BrowserView):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def get_authors(self):
        res = ""
        if (hasattr(self.context, 'authors')):
            for author in self.context.authors:
                if(author == self.context.authors[-1]):
                    res += ' and'
                elif (author != self.context.authors[0]):
                    res += ','

                res += f' {author["firstname"]}'
                if ('middlename' in author and author['middlename'] != None):
                    res += f' {author["middlename"]}'
                res += f' {author["lastname"]}'

            return res.strip()
        return ""
