from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getUtility
from html.parser import HTMLParser
from example.bibliography_folder.interfaces import IExampleBibliographyFolderUtility

from example.bibliography_folder.config import REFERENCE_TYPES
from example.bibliography_folder import _
from zope.security import checkPermission

# TODO:
# - [x] Make the get_authors() method check if there are more than 6 authors
# - [x] Implement/port get_formatted_identifiers
# - [ ] Add special formatting if the entry is an IncollectionReference, etc.
# - [ ] Add the filter class


class BibliographyFolder(BrowserView):
    index = ViewPageTemplateFile("../templates/folder.pt")
    filter_settings = {'filter_by_year': True}
    query = {}
    author_objects = []

    def debug_return_list(self):
        res = ""
        for i in self.context.portal_catalog(self.query):
            res += i.getURL() + "<br/>"

        return res

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.gutil = getUtility(IExampleBibliographyFolderUtility)

    def __call__(self):
        self.initialize()
        return self.index()
        
    def initialize(self):
        self.query = {
            'path': '/'.join(self.context.getPhysicalPath()),
            'sort_on': 'publication_year'
        }

        if self.request.get('filter.author', None) is None:
            self.request.set('filter.author', self.filter_settings.get('filter_default_author', '').replace(',', ''))

        if self.request.get('filter.year', None) is None:
            self.request.set('filter.year', self.filter_settings.get('filter_default_year', ''))

        author = self.request.get('filter.author', '').strip().replace(',', ' ')
        author = filter(None, author.split(' '));
        author = list(map(str.strip, author))

        if author and author[0]:
           #query['SearchableText'] = author[0];
           self.query['author_items'] = ' '.join(author);

        year = self.request.get('filter.year', '').strip()
        if year:
            self.query['publication_year'] = self.gutil.publication_year_query_value(year);


    def run_query(self):
        results = self.context.portal_catalog(self.query)

        # from Products.CMFPlone import Batch
        # results = Batch(contents, 0, 1000, orphan=0)
        return self.group_bib_items_by_year(results)

    # In the original GEMI implementation this was used inside a utility class but im not
    # sure where else this might get used again and due to problems with getUtility I put it
    # here for now.
    def group_bib_items_by_year(self, list):
        """ Takes a list of Brains and returns a dictionary with keys being the years
        """
        res = {}
        for r in list:
            year = r.publication_year.lower().title()
            if not year in res:
                res[year] = [r]
            else: 
                res[year].append(r)

        return res

    def is_valid_year(self, year):
        year = year.lower().strip()
        if (year.isdigit()):
            return float(year) > 1900
        return year

    def can_add_content(self):
        return checkPermission('cmf.AddPortalContent', self.context)

class ViewListFormatter(BrowserView):
    item_template = ViewPageTemplateFile("../templates/folder_item.pt")

    def __init__(self, context, request):
        self.htmlparser = HTMLParser()
        self.context = context
        self.request = request
        self.gutil = getUtility(IExampleBibliographyFolderUtility)

    def __call__(self, item=None):
        if(item is None):
            return None
        self.item = item
        self.item_object = self.item.getObject()
        return self.item_template()

    def is_correct_portal_type(self):
        # if (self.item.portal_type in REFERENCE_TYPES):
        #     return True
        # return False
        return True

    # TODO: Implement the specification for when there are more than 6 authors
    def get_authors(self):
        objects = self.gutil.get_author_objects(self.item_object)
        if (objects):

            _list = []

            if (len(objects) > 5):
                new_objects = objects[:6]
                new_objects.append(objects[len(objects) - 1]) 
                objects = new_objects
                
            for i, author in enumerate(objects):
                _a = author.get('lastname') 
                if author.get('firstname'):
                    _a += ', '
                    _a += author.get('firstname') + '.'
                if author.get('middlename'):
                    _a += ' ' + author.get('middlename') + '.'
                if author.get('eq_contrib'):
                    _a += '*'
                if author.get('print_eq_contrib'):
                    pass #_a += ' [*' + _('equally contributing') + ']'
                if len(_list) > 0:
                    _a = ', ' + _a

                _list.append(_a)

            if len(_list) > 1:
                lastIndex = len(_list) - 1
                if (len(objects) > 6):
                    _list[lastIndex] = ', ...' + _list[lastIndex].strip(',')
                else:
                    _list[lastIndex] = ' & ' + _list[lastIndex].strip(',')

            return ''.join(_list)
        return None

    def get_title(self):
        return self.item_object.title.strip(".") + "."

    def get_publication_year(self):
        return self.item_object.publication_year.strip()

    def get_journal(self):
        if not self.item_object.journal:
            return False
        journal = self.item_object.journal
        if self.format_volume():
            journal += ', '
        else:
            journal += '.'

        return journal

    def format_volume(self):
        item = self.item_object
        v = ''
        if (getattr(item, 'volume', None)):
            v += "<i>%s</i>" % item.volume
        if (getattr(item, 'number', None)):
            v += '('+ item.number +')'
        v = v.strip()
        if v and self.get_pages():
            v = v + ', '
        elif v and not self.get_pages():
            v = v + '.'

        return v

    def get_pages(self):
        if (getattr(self.item_object, 'pages', None)):
            return self.htmlparser.unescape(self.item_object.pages) + '.'
        return None

    def get_pdf_url(self):
        if not(hasattr(self.item_object.pdf_file, 'filename')):
            return False
        filename = self.item_object.pdf_file.filename.lower().replace(' ', '-')
        path = '/'.join(self.item_object.getPhysicalPath()[:-1])+ '/PDFs/'+ filename
        url = path + '/@@display-file/file/'
        return url

    def get_formatted_identifiers(self):
        if (not hasattr(self.item_object, 'identifiers')):
            return None
        try:
            s = ' '.join([self.get_formatted_identifier(identifier) for identifier in self.item_object.identifiers]).strip(',').strip();
            if s:
                return s + '.';
        except:
            return None;

    def get_formatted_identifier(self, identifier):
        if (identifier['identifier_type'] == 'DOI'):
            idf = identifier['identifier_value'] if ("doi.org" in identifier['identifier_value']) else "https://dx.doi.org/" + identifier['identifier_value']
            return " %s," % idf
        else:
            return " %s: %s," % (identifier['identifier_type'], identifier['identifier_value'])

class Filter(BrowserView):
    template = ViewPageTemplateFile("../templates/folder_filter.pt")

    def __init__(self, context, request):
        self.htmlparser = HTMLParser()
        self.context = context
        self.request = request
        self.filter_settings = {}
        self.gutil = getUtility(IExampleBibliographyFolderUtility)

    def __call__(self, settings=None):
        if(settings is None):
            return None
        self.filter_settings = settings
        return self.template()

    def get_author_list(self):
        objects = self.gutil.get_all_bib_author_objects(self.context)
        res = []
        for item in objects:
            res.append("%s, %s" % (item['lastname'], item['firstname']))

        return res

    
    def get_years_list(self):
        years = self.gutil.get_all_bib_years(self.context)

        if (len(years) > 1):
            years = [_('')] + years;
        return years
