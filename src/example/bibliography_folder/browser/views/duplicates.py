from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class DuplicatesManagementView(BrowserView):
    template = ViewPageTemplateFile("../templates/duplicates.pt")

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.duplicates_criteria = self.context.get_duplicates_criteria()

    def __call__(self):
        if not self.request.form.get('form.submitted'):
            return self.template()

        new_duplicates_criteria = {}
        criteria_map = self.context.get_available_criteria()
        for contenttype in list(criteria_map.keys()):
            for field in criteria_map[contenttype]:
                if self.request.get(self.get_fields_checkbox_name(contenttype, field)):
                    if not contenttype in new_duplicates_criteria:
                        new_duplicates_criteria[contenttype] = []
                    new_duplicates_criteria[contenttype].append(field)

        self.context.set_duplicates_criteria(new_duplicates_criteria)
        return self.template()

    def is_field_checked(self, contenttype, field):
        """
        Returns 'checked' if criteria field is stored in '_duplicates_criteria' in the
        criteria manager, ergo is select and supposed to be checked.
        If not it returns None.
        """
        if len(self.duplicates_criteria) > 0 and field in self.context.get_duplicates_criteria()[contenttype]:
            return "checked"
        return None

    def get_fields_checkbox_name(self, contenttype, field):
        """
        Returns name used for the fields corresponding checkbox in the template. This
        function exists mostly for consistency purposes.
        """
        return contenttype + "_" + field
