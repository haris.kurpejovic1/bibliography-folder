from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getUtility
from html.parser import HTMLParser
from example.bibliography_folder.interfaces import IExampleBibliographyFolderUtility

from example.bibliography_folder import _

from example.bibliography_folder.config import PARSER_FORMATS
from plone.dexterity.utils import createContentInContainer

from plone.namedfile.utils import set_headers
from plone.namedfile.utils import stream_data
from Products.Five import BrowserView
from zope.publisher.interfaces import NotFound
from plone.namedfile.file import NamedBlobFile

class ExportView(BrowserView):
    template = ViewPageTemplateFile("../templates/export.pt")

    def __init__(self, context, request):
        self.gutil = getUtility(IExampleBibliographyFolderUtility)
        self.context = context
        self.request = request

    def __call__(self):
        if not self.request.form.get('form.submitted'):
            return self.template()


        self.errors = {}
        parser_format = self.request.form.get('format', 'bibtex')
        eol_style = self.request.form.get('eol_style', 'nope')

        bib_objects = self.gutil.get_all_bib_child_objects(self.context, True)
        output = PARSER_FORMATS[parser_format]['parser']().reverse_parse(bib_objects)
        if eol_style:
            output.replace('\n', '\r\n')
        blob_data = NamedBlobFile(output, filename='export.bib')
        print(repr(output))

        # Sets Content-Type and Content-Length
        self.request.response.setHeader('Content-Type', 'application/octet-stream')
        set_headers(blob_data, self.request.response)

        # Set Content-Disposition
        self.request.response.setHeader(
            'Content-Disposition',
            'inline; filename={0}'.format('export.bib')
        )
        return stream_data(blob_data)
