from plone import api
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getUtility
from html.parser import HTMLParser
from example.bibliography_folder.interfaces import IExampleBibliographyFolderUtility
from Products.CMFCore.utils import getToolByName

from example.bibliography_folder import _

from example.bibliography_folder.config import PARSER_FORMATS
from plone.dexterity.utils import createContentInContainer
import sys
import logging

logger = logging.getLogger("example.bibliography_folder import")

# TODOs
# - [x] Fix the template regarding the format
# - [ ] Implement the report building
# - [ ] Add the logging
# - [x] Add DOI assignment to process_single_import()


class ImportView(BrowserView):
    template = ViewPageTemplateFile("../templates/import.pt")

    def __init__(self, context, request):
        self.gutil = getUtility(IExampleBibliographyFolderUtility)
        self.context = context
        self.request = request

    def __call__(self):
        self.processed = False
        self.errors = {}
        start_time = self.context.ZopeTime().timeTime()
        if not self.request.form.get('form.submitted'):
            return self.template()

        # fetch value from request
        input_encoding = self.request.form.get('input_encoding', 'UTF-8')
        # span_of_search = self.request.form.get('span_of_search', None)

        parser_format = self.request.form.get('format', 'bibtex')

        logger.info("Importing with input_encoding: " + str(input_encoding) + ", parser_format: " + str(parser_format))

        # Fetch the input/source from either the file or the text field
        filename = None
        source = self.request.form.get('up_text')

        if not source:
            upfile = self.request.form.get('file')
            filename = upfile and getattr(upfile, 'filename', None)
            if not filename:
                self.errors['file'] = _("You must import a file or enter a text.")
                return self.template()
            source = str(upfile.read())
            print(source)
            print(str(type(source)))
            if not source or not type(source) == str:
                msg = "Could not read the file '%s'." % filename
                self.errors['file'] = msg
                return self.template()
        else:
            logger.info("DEBUG: Going with uploaded file")

        # Skip DOS line breaks
        source = source.replace('\r', '')

        # Get the extracted entries from the parser as a list
        # try:

        logger.info("DEBUG: Attempting to use parser with key '" + parser_format + "'")
        entries = PARSER_FORMATS[parser_format]['parser']().parse_string(source)

        # Start building the report
        mtool = getToolByName(self.context, 'portal_membership')
        member = mtool.getAuthenticatedMember()
        fullname = member.getProperty('fullname', None)
        if fullname:
            username = "%s (%s)" % (fullname, member.getId())
        else:
            username = member.getId()
        
        tmp_report = '[%s] Imported by %s' % (self.context.ZopeTime(),
                                              username)
        if filename is not None:
            tmp_report += ' from file %s' % filename
            tmp_report += ':\n\n'


        counter = 0
        processed_entries = 0
        import_errors = 0


        logger.info("Start import of %s raw entries" % len(entries))
        # Process the imported entries
        for entry in entries:
            counter += 1
            count = '#%05i:' % counter
            logger.info(count + "Processing Entry")
            if isinstance(entry, str):
                msg = "Entry could not be parsed! %s" % entry
                upload = (msg, "error")
                logger.error(count + msg)
            elif entry.get('title'):
                logger.info(count + "Successful normal processing of " + entry['title'])
                upload = self.process_single_import(entry)
            else:
                formated = '; '.join(['%s=%s' % (key, entry[key])
                                      for key in sorted(entry.keys())
                                      if key == key.lower()])

                upload = ("Found entry without title: %s\n" % formated, "error")
                logger.error(count + upload[0])

            if upload[1] == "ok":
                processed_entries += 1
            else:
                import_errors += 1

            state, msg = upload[1].upper(), upload[0]
            tmp_report = "%s: %s\n" % (state, msg)
        msg = "Processed %i entries. There were %i errors. "\
            "Import processed in %f seconds. See import report below." \
            % (processed_entries, import_errors,
                self.context.ZopeTime().timeTime() - start_time)
        logger.info(msg)

        logger.info(tmp_report)

        return self.template()

    # Not sure if this method belongs here or just directly in the folder content type
    def process_single_import(self, entry, container_path=None):
        container_path = container_path or self.context
        import_status = "ok"
        try:
            if (entry['ENTRYTYPE'] == 'article'):
                item = createContentInContainer(container_path, "ArticleReference", title=entry['title'])
                item.assign_values_from_entry(entry)
        except:
            import_status = "Error type: %s. Error value: %s." \
                % (sys.exc_info()[0], sys.exc_info()[1])

        return ("put_report_line", import_status)
