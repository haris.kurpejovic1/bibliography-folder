# -*- coding: utf-8 -*-
from example.bibliography_folder.testing import EXAMPLE_BIBLIOGRAPHY_FOLDER_INTEGRATION_TESTING  # noqa
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class BibliographyFolderIntegrationTest(unittest.TestCase):

    layer = EXAMPLE_BIBLIOGRAPHY_FOLDER_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.parent = self.portal

    def test_ct_bibliography_folder_schema(self):
        fti = queryUtility(IDexterityFTI, name='Bibliography Folder')
        schema = fti.lookupSchema()
        schema_name = portalTypeToSchemaName('Bibliography Folder')
        self.assertEqual(schema_name, schema.getName())

    def test_ct_bibliography_folder_fti(self):
        fti = queryUtility(IDexterityFTI, name='Bibliography Folder')
        self.assertTrue(fti)

    def test_ct_bibliography_folder_factory(self):
        fti = queryUtility(IDexterityFTI, name='Bibliography Folder')
        factory = fti.factory
        obj = createObject(factory)


    def test_ct_bibliography_folder_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.portal,
            type='Bibliography Folder',
            id='bibliography_folder',
        )


        parent = obj.__parent__
        self.assertIn('bibliography_folder', parent.objectIds())

        # check that deleting the object works too
        api.content.delete(obj=obj)
        self.assertNotIn('bibliography_folder', parent.objectIds())

    def test_ct_bibliography_folder_globally_addable(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        fti = queryUtility(IDexterityFTI, name='Bibliography Folder')
        self.assertTrue(
            fti.global_allow,
            u'{0} is not globally addable!'.format(fti.id)
        )

    def test_ct_bibliography_folder_filter_content_type_false(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        fti = queryUtility(IDexterityFTI, name='Bibliography Folder')
        portal_types = self.portal.portal_types
        parent_id = portal_types.constructContent(
            fti.id,
            self.portal,
            'bibliography_folder_id',
            title='Bibliography Folder container',
         )
        self.parent = self.portal[parent_id]
        obj = api.content.create(
            container=self.parent,
            type='Document',
            title='My Content',
        )
        self.assertTrue(
            obj,
            u'Cannot add {0} to {1} container!'.format(obj.id, fti.id)
        )
