# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s example.bibliography_folder -t test_article_reference.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src example.bibliography_folder.testing.EXAMPLE_BIBLIOGRAPHY_FOLDER_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot /src/example/bibliography_folder/tests/robot/test_article_reference.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a ArticleReference
  Given a logged-in site administrator
    and an add Bibliography Folder form
   When I type 'My ArticleReference' into the title field
    and I submit the form
   Then a ArticleReference with the title 'My ArticleReference' has been created

Scenario: As a site administrator I can view a ArticleReference
  Given a logged-in site administrator
    and a ArticleReference 'My ArticleReference'
   When I go to the ArticleReference view
   Then I can see the ArticleReference title 'My ArticleReference'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add Bibliography Folder form
  Go To  ${PLONE_URL}/++add++Bibliography Folder

a ArticleReference 'My ArticleReference'
  Create content  type=Bibliography Folder  id=my-article_reference  title=My ArticleReference

# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.IBasic.title  ${title}

I submit the form
  Click Button  Save

I go to the ArticleReference view
  Go To  ${PLONE_URL}/my-article_reference
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a ArticleReference with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the ArticleReference title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
