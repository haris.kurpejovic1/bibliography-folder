# -*- coding: utf-8 -*-
from plone.app.contenttypes.testing import PLONE_APP_CONTENTTYPES_FIXTURE
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import (
    applyProfile,
    FunctionalTesting,
    IntegrationTesting,
    PloneSandboxLayer,
)
from plone.testing import z2

import example.bibliography_folder


class ExampleBibliographyFolderLayer(PloneSandboxLayer):

    defaultBases = (PLONE_APP_CONTENTTYPES_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.restapi
        self.loadZCML(package=plone.restapi)
        self.loadZCML(package=example.bibliography_folder)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'example.bibliography_folder:default')


EXAMPLE_BIBLIOGRAPHY_FOLDER_FIXTURE = ExampleBibliographyFolderLayer()


EXAMPLE_BIBLIOGRAPHY_FOLDER_INTEGRATION_TESTING = IntegrationTesting(
    bases=(EXAMPLE_BIBLIOGRAPHY_FOLDER_FIXTURE,),
    name='ExampleBibliographyFolderLayer:IntegrationTesting',
)


EXAMPLE_BIBLIOGRAPHY_FOLDER_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(EXAMPLE_BIBLIOGRAPHY_FOLDER_FIXTURE,),
    name='ExampleBibliographyFolderLayer:FunctionalTesting',
)


EXAMPLE_BIBLIOGRAPHY_FOLDER_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        EXAMPLE_BIBLIOGRAPHY_FOLDER_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name='ExampleBibliographyFolderLayer:AcceptanceTesting',
)
