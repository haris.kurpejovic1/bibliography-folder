from example.bibliography_folder import _
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from example.bibliography_folder.config import REFERENCE_TYPES
from example.bibliography_folder.config import IDENTIFIER_TYPES

def contenttypes():
    return SimpleVocabulary(
        [SimpleVocabulary.createTerm(t, _(t), t) for t in list(REFERENCE_TYPES)]
    )

def identifiertypes():
    return SimpleVocabulary([SimpleVocabulary.createTerm(t, _(t), t) for t in list(IDENTIFIER_TYPES)])
