import logging
from Products.CMFPlone.utils import _createObjectByType

class BibliographyFolderCreated:
    
    def __init__(self, folder, event):
        self.folder = folder
        self.event = event

    def create_pdf_folder(self):
        _createObjectByType("PdfFolder", self.folder, "PDFs")
    
    def check_duplicates(self):
        pass
