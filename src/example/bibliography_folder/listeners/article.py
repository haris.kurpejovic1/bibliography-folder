import logging
from plone.dexterity.utils import createContentInContainer
from plone.namedfile import NamedFile
from plone import api

class ArticleReferenceCreated:
    def __init__(self, article, event):
        self.article = article
        self.event = event

    def check_and_create_pdf_file(self):
        if (hasattr(self.article.pdf_file, "filename")):
            folders = api.content.find(depth = 1, context=self.article.__parent__, portal_type='PdfFolder')

            if (len(folders) >= 1):
                item = createContentInContainer(folders[0].getObject(), "PdfFile", title=self.article.pdf_file.filename)
                item.file = NamedFile(self.article.pdf_file.data, filename=self.article.pdf_file.filename)
