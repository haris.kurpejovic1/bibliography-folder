import bibtexparser
from bibtexparser.bibdatabase import BibDatabase
import re
import example.bibliography_folder.config

class BibTeXParser():
    def parse_string(self, text_string):
        entries = bibtexparser.loads(text_string).entries
        for entry in entries:
            authors = []
            author_strings = entry['author'].split('and')
            for author in author_strings:
                author_obj = {}
                parts = author.split(',')
                author_obj['lastname'] = parts[0].strip()
                if parts[1].find('.') != -1:
                    author_obj['firstname'] = parts[1].split('.')[0].strip()
                else:
                    author_obj['firstname'] = parts[1].strip().split(' ')[0]


                authors.append(author_obj)
            entry['authors'] = authors

            identifiers = []
            for identifier_key in example.bibliography_folder.config.IDENTIFIER_TYPES:
                if identifier_key.lower() in entry:
                    identifier = entry[identifier_key.lower()]
                    if identifier.startswith(identifier_key.lower() + ':'):
                        length = len(identifier) + 1
                        identifier = identifier[length:]
                    identifiers.append({
                        'identifier_type': identifier_key,
                        'identifier_value': identifier
                    })
            entry['identifiers'] = identifiers
        return entries

    # WIP method name :)
    # This method assumes it only gets passed a list of ArticleReferences and
    # will need some restructuring when new content types get introduced.
    def reverse_parse(self, item_list):
        database = BibDatabase()
        database.entries = []

        for item in item_list:
            entry = {
                'title': item.title,
                'year': item.publication_year,
                "ENTRYTYPE": 'article',
                "ID": re.sub('[^A-Za-z0-9 ]+', '', item.title).lower().strip().replace(' ', '-')
            }
            if(hasattr(item, 'authors')):
                author_string = ""
                for author in item.authors:
                    if(author != item.authors[0]):
                        author_string += ' and '
                    author_string += f'{author["lastname"]}, {author["firstname"]}'
                    if ((middlename := author.get("middlename"))):
                        author_string += f' {middlename}'
                    author_string.strip()
                entry['author'] = author_string

            if(hasattr(item, 'identifiers')):
                for identifier in item.identifiers:
                    entry[identifier['identifier_type'].lower()] = identifier['identifier_value']

            if(hasattr(item, 'journal')):
                entry['journal'] = str(item.journal)

            if(hasattr(item, 'number')):
                entry['number'] = str(item.number)

            if(hasattr(item, 'pages')):
                entry['pages'] = str(item.pages)
                
            if(hasattr(item, 'volume')):
                entry['volume'] = str(item.volume)

            if(hasattr(item, 'abstract')):
                entry['abstract'] = str(item.abstract)

            database.entries.append(entry)

        if(len(database.entries) > 0):
            output = bibtexparser.dumps(database)
            return output
        return None

