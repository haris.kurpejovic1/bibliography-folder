from example.bibliography_folder.listeners.folder import BibliographyFolderCreated
from example.bibliography_folder.listeners.article import ArticleReferenceCreated

def bibliography_folder_created(folder, event):
    listener = BibliographyFolderCreated(folder, event)
    listener.create_pdf_folder()
    listener.check_duplicates()


def article_reference_created(article, event):
    listener = ArticleReferenceCreated(article, event)
    listener.check_and_create_pdf_file()
