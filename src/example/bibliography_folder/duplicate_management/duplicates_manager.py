from plone.dexterity.utils import createContentInContainer
from Products.CMFPlone.utils import _createObjectByType

class BibliographyFolderDuplicatesManager:
    _duplicates = []

    _duplicates_folder_name = 'duplicates'

    def __init__(self):
        pass

    # TODO: This needs to get restructured a bit, since the 'duplicates_matching_policy' field
    #       will have different values based on the set language. It should probably use a translatable
    #       vocabulary instead. This is just a rudimentary setup to get some stuff working.
    def get_duplicates_matching_policy(self):
        if self.duplicates_matching_policy == "This Plone site":
            return 'global'
        return 'local'

    def get_duplicates_folder(self, id='duplicates'):
        """
        Returns the folder duplicates get stored in, creates one if it does
        note exist already.
        """
        cat = self.portal_catalog
        query = {
            'path': '/'.join(self.getPhysicalPath()),
            'id': id
        }
        if len((res := cat(query))) > 0:
            return res[0].getObject()
        return _createObjectByType("DuplicatesFolder", self, id)
    
    # Maybe rename to 'get_duplicates()'
    def _get_duplicates(self, item, span_of_search):
        """
        Checks if the passed item already exists in the specified scope. 
        The scope can either be 'local', checking inside the local folder
        instance, or 'global', checking the entire folder.
        
        Returns the already existing duplicate objects if they exist, otherwise false.
        """
        cat = self.portal_catalog

        query = {
            'portal_type': item.portal_type
        }
        if span_of_search == 'local':
            query['path'] = '/'.join(self.getPhysicalPath())
        elif span_of_search == 'global':
            query['path'] = '/'
        else:
            raise ValueError("span of search for duplicates has an " +
                             "invalid value : %s" % span_of_search)

        # Collect all objects with the same portal_type
        brains = cat(query)
        if (len(cat(brains)) < 1):
            return False

        objs = [brain.getObject() for brain in brains]
        duplicates = []

        for obj in objs:
            is_duplicate = True
            for criteria in self.get_duplicates_criteria()[item.portal_type]:
                if getattr(item, criteria) != getattr(obj, criteria):
                    is_duplicate = False
            if is_duplicate:
                duplicates.append(obj)

            
        return duplicates
