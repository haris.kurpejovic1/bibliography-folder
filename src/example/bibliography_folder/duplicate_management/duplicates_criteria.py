from zope.schema import getFieldsInOrder

class BibliographyFolderDuplicatesCriteriaManager:
    # Contains the possible criteria from every bibliograhpy type
    _criteria =  {}
    # Contains criteria that should not be available to choose to check for
    _ignored_criteria = ['id']


    # Contains the default duplicates criteria
    _default_criteria = (
        'authors',
        'portal_type',
        'publication_year',
        'title',
    )

    # Contains criteria to be checked for duplication while importing for each bibliograhpy type
    _duplicates_criteria = {}

    def get_duplicates_criteria(self):
        if len(self._criteria) == 0:
            self.init_criteria()

        return self._duplicates_criteria

    def set_duplicates_criteria(self, duplicates_map):
        self._duplicates_criteria = duplicates_map

    def get_available_criteria(self):
        if len(self._criteria) == 0:
            self.init_criteria
            
        return self._criteria

    # TODO: Sort criteria by their i18n title
    def sort_criteria_by_title(self):
        pass

    def get_all_fields_from_schema(self, schema):
        """
        Returns a list with all field names as Strings from the passed schema.
        Also looks through all provided behavior.
        """
        fields = [x[1] for x in getFieldsInOrder(schema)]
        return [x for x in fields if x not in self._ignored_criteria]
    
    def init_criteria(self):
        self._criteria =  {
            'ArticleReference': [
                'authors',
                'publication_year',
                'journal',
                'volume',
                'number',
                'pagers',
                'identifiers',
                'publication_month',
                'note',
                'annotation'
            ],
            'ExampleReference': [
                'authors',
                'publication_year',
                'journal',
                'volume',
                'number',
                'pagers',
                'identifiers',
                'publication_month',
                'note',
                'annotation'
            ]
        }

        for key in self._criteria:
            self._duplicates_criteria[key] = []
            self._duplicates_criteria[key] += list(self._default_criteria)

