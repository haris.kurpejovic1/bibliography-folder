# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from zope.interface import Interface


class IExampleBibliographyFolderLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""
    pass


class IExampleBibliographyFolderUtility(Interface):
    """Marker interface for utility object."""
