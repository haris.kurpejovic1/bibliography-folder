# -*- coding: utf-8 -*-
from plone.dexterity.content import Container
from plone.supermodel import model
from example.bibliography_folder import _
from example.bibliography_folder.vocabularies.vocabulary import contenttypes
from zope import schema
from plone.autoform import directives
from plone.supermodel.directives import fieldset
from plone.autoform.interfaces import IFormFieldProvider
from z3c.form.browser.radio import RadioFieldWidget
from zope.interface import implementer
from zope.interface import provider
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary
from Persistence import PersistentMapping

from example.bibliography_folder.duplicate_management.duplicates_criteria import BibliographyFolderDuplicatesCriteriaManager
from example.bibliography_folder.duplicate_management.duplicates_manager import BibliographyFolderDuplicatesManager


cooking_method_items = ["Abbreviation Bibliography ID Cooker", "EtAl Bibliography ID Cooker", "PLONE Default Bibliography ID Cooker", "UID Bibliography ID Cooker"]
cooking_method_vocabulary = SimpleVocabulary.fromValues(cooking_method_items)

"""
pdf_attachment_items = ["ArticleReference", "BookletReference", "BookReference", "ConferenceReference", "InbookReference", "IncollectionReference"]
pdf_attachment_vocabulary = SimpleVocabulary.fromValues(pdf_attachment_items)
"""

@provider(IFormFieldProvider)
class IBibliographyFolder(model.Schema):
    """ Marker interface for BibliographyFolder
    """
    searchable = schema.Bool(
        title=_("Searchable references"),
        description=_("Let the bibliography search form find this folder's\
                        references. Also let the references appear in (Smart)\
                        Bibliography Lists, if installed."),
        default=True,
    )

    cook_ids_on_bib_ref_creation = schema.Bool(
        title=_("Meaningful short names"),
        description=_("Give references short names like \"Smith1980\" or\
                        \"effects-of-vacations-on-mathematicians\" rather than\
                        \"bookreference.2008-02-04.5603119945\""),
    )

    reference_id_cooking_method = schema.Choice(
        title=_("Short name cooker"),
        description=_("How to format short names, if Meaningful short names\
                        is on. If unsure which to choose, leave as is."),

            vocabulary=cooking_method_vocabulary
        # default=_("EtAl Bibliography ID Cooker"),
            )

    cook_ids_after_bib_ref_edit = schema.Bool(
        title=_("Keep short names up to date"),
        description=_("Update the short names of references if their\
                        contents change. Note that this can cause URLs of\
                        references to change, breaking external links."),
    )

    use_parser_ids_on_import = schema.Bool(
        title=_("Use short names from imported files"),
        description=_("Some importable files already associate a unique\
                    identifier with each reference. These can be used as\
                        short names for imported references."),
        default=True,
    )

    enable_duplicates_manager = schema.Bool(
        title=_("Duplicate management"),
        description=_("Treat duplicate references specially upon import,\
                        cut, copy, or paste. This is recommended."),
    )

    directives.widget(duplicates_matching_policy=RadioFieldWidget)
    duplicates_matching_policy = schema.Choice(
        title=_("Duplicates Matching Policy"),
        description=_("If duplicates management is enabled, this\
                        value will define the search span for matching\
                        objects."),
        values=[_("This folder"),_("This Plone site")],
        default=_("This folder"),
    )

    # form.widget(testlist=textlines)
    test_list = schema.List(
        title=_("Allow PDF uploads for..."),
        description=_("The selected types of references will accept PDF\
                        attachements"),
        value_type=schema.Choice(
            title=_("Test"),
            description=_("Bibliographical reference types of this\
                            bibliography folder will only support upload of\
                            publications in PDF format if selected here."),
            vocabulary=contenttypes(),
            required=False,
        ),
    )

    synchronize_pdf_file_attributes = schema.Bool(
        title=_("Synchronize PDF file attributes"),
        description=_("Synchronize some attributes of an uploaded PDF file\
                        with the attribute values of the referring\
                        bibliographical entry (currently supported: ID, Roles,\
                        Creator)."),
    )



@implementer(IBibliographyFolder)
class BibliographyFolder(Container, BibliographyFolderDuplicatesCriteriaManager, BibliographyFolderDuplicatesManager):
    pass

