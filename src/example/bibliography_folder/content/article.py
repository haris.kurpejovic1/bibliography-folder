# -*- coding: utf-8 -*-
# from plone.app.textfield import RichText
from plone.autoform import directives
# from plone.namedfile import field as namedfile
from plone.supermodel import model
# from plone.supermodel.directives import fieldset
# from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer
from example.bibliography_folder.content.base import BaseEntry
from example.bibliography_folder.content.base import IBibliographyItem
from example.bibliography_folder import _
from example.bibliography_folder.behaviors.author import IAuthorSchema
from example.bibliography_folder.behaviors.fields import IIdentifierSchema
from plone.autoform.interfaces import IFormFieldProvider
from zope.interface import provider

@provider(IFormFieldProvider)
class IArticleReference(IBibliographyItem):
    """ Marker interface and Dexterity Python Schema for ArticleReference
    """

@implementer(IArticleReference)
class ArticleReference(BaseEntry):
    """
    """

    def assign_values_from_entry(self, entry):
        self.publication_year = entry.get('year')
        self.journal = entry.get('journal')
        self.abstract = entry.get('abstract')
        self.description = entry.get('abstract')
        self.number = entry.get('number')
        self.pages = entry.get('pages')
        self.volume = entry.get('volume')
        self.authors = entry.get('authors')
        self.identifiers = entry.get('identifiers')

