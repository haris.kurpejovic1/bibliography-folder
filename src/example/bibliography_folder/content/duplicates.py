# -*- coding: utf-8 -*-
from plone.dexterity.content import Container
from plone.supermodel import model
from example.bibliography_folder import _
from plone.autoform.interfaces import IFormFieldProvider
from zope.interface import provider
from zope.interface import implementer

@provider(IFormFieldProvider)
class IDuplicatesFolder(model.Schema):
    """ Marker interface for DuplicatesFolder
    """

@implementer(IDuplicatesFolder)
class DuplicatesFolder(Container):
    pass
