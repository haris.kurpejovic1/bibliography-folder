# -*- coding: utf-8 -*-
from plone.dexterity.content import Container
from plone.supermodel import model
from example.bibliography_folder import _
from plone.autoform.interfaces import IFormFieldProvider
from zope.interface import provider
from zope.interface import implementer
from plone.namedfile.field import NamedFile

from plone.autoform import directives
from zope.interface import Invalid

def pdf_filetype_constraint(value):
    """ Check if the supplied filetype is a PDf.
    """
    if not (value.contentType == 'application/pdf'):
        raise Invalid("Please only upload Pdfs.")
    return True

@provider(IFormFieldProvider)
class IPdfFolder(model.Schema):
    """ Marker interface for PdfFolder
    """

@implementer(IPdfFolder)
class PdfFolder(Container):
    pass

@provider(IFormFieldProvider)
class IPdfFile(model.Schema):
    """ Marker interface for PdfFile
    """
    # directives.omitted('description')
    file = NamedFile(
        title=_("Please upload a PDF file."),
        required=False,
        constraint=pdf_filetype_constraint,
    )
