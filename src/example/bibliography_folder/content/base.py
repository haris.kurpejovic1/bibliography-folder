from plone.dexterity.content import Item
from plone.supermodel import model
from example.bibliography_folder import _
from example.bibliography_folder.helpers import format_author_name
from zope import schema
from plone.autoform import directives
from plone.supermodel.directives import fieldset
from plone.autoform.interfaces import IFormFieldProvider
from zope.interface import provider


@provider(IFormFieldProvider)
class IBibliographyItem(model.Schema):
    """
    Base interface for a bibliography content item
    """

    """
    Add a dummy hidden filed to move full-text fieldset after the default fieldset
    """
    directives.mode(hidden='hidden')
    hidden = schema.TextLine(
        title=_(u'hidden'),
        required=False
    )

    fieldset(
        'full-text',
        fields=('hidden',),
    )


class BaseEntry(Item):
    """
    Base class for a bibliography content item
    """

    def author_items(self, format="{L} {f}"):
        if self.authors is None:
            return []

        return [format_author_name(a) for a in self.authors]

